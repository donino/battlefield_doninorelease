#ifndef SIZE_OF_WINDOW_H
#define SIZE_OF_WINDOW_H

#include <gtk/gtk.h>
#include <cairo.h>
#include "timefunctionplot.h"

void get_size (GtkWindow *, GdkEvent *, TimeFunctionPlot *);

#endif /* include guard SIZE_OF_WINDOW_H */
