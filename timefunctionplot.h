#ifndef TIME_FUNCTION_PLOT_H
#define TIME_FUNCTION_PLOT_H

#include <gtk/gtk.h>
#include <cairo.h>

typedef struct {
	int		 rear;
	float	 data[500];
}CircQueue;

typedef struct {
	GtkWidget *envelope;
	int        scale_x;
	int        scale_y;
	int        zero_x;
	int        zero_y;
	CircQueue *data; 
	int        last_event_x;
	int        last_event_y;
	int        width;
	int        height;
	gboolean   play;
	gboolean   continue_play;
	char      *path;
}TimeFunctionPlot;

TimeFunctionPlot *tfp_new(CircQueue *);
GtkWidget        *tfp_get_enveloping_widget(TimeFunctionPlot*);

#endif /* include guard TIME_FUNCTION_PLOT_H */
