#ifndef MOVING_WITH_MOUSE_H
#define MOVING_WITH_MOUSE_H

#include <gtk/gtk.h>
#include "timefunctionplot.h"

gboolean on_button_press (GtkWidget *, GdkEventButton *, TimeFunctionPlot *);
gboolean motion_event (GtkWidget *, GdkEventMotion *, TimeFunctionPlot *);

#endif /* include guard MOVING_WITH_MOUSE_H */
