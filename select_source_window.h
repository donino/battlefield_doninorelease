#ifndef SELECT_SOURCE_WINDOW_H
#define SELECT_SOURCE_WINDOW_H

#include <gtk/gtk.h>

GtkWidget *select_source (TimeFunctionPlot *);

#endif /* include guard SELECT_SOURCE_WINDOW_H */
