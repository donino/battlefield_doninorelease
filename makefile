CFLAGS = `pkg-config --cflags gtk+-3.0`
LFLAGS = `pkg-config --libs gtk+-3.0` -lm

all: main.o timefunctionplot.o accelerometer_data.o select_source_window.o moving_with_mouse.o size_of_window.o
	gcc main.o timefunctionplot.o accelerometer_data.o select_source_window.o moving_with_mouse.o size_of_window.o -o front $(LFLAGS)

main.o: main.c
	gcc $(CFLAGS) -c main.c

timefunctionplot.o: timefunctionplot.c timefunctionplot.h
	gcc $(CFLAGS) -c timefunctionplot.c

accelerometer_data.o: accelerometer_data.c accelerometer_data.h
	gcc $(CFLAGS) -c accelerometer_data.c

select_source_window.o: select_source_window.c select_source_window.h
	gcc $(CFLAGS) -c select_source_window.c

moving_with_mouse.o: moving_with_mouse.c moving_with_mouse.h
	gcc $(CFLAGS) -c moving_with_mouse.c

size_of_window.o: size_of_window.c size_of_window.h
	gcc $(CFLAGS) -c size_of_window.c

clean:
	rm -f *.o
	rm -f front
