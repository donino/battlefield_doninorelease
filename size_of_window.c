#include <gtk/gtk.h>
#include <cairo.h>
#include "timefunctionplot.h"

void get_size (GtkWindow *window, GdkEvent *event, TimeFunctionPlot *tfp){
	int width, height;

	gtk_window_get_size (window, &width, &height);

	//inicializacia
	tfp->zero_x = 30;
	tfp->zero_y = height / 2;

	//za dolnu ciarku surova velkost
	tfp->width = tfp->zero_x;
	tfp->height = tfp->zero_y;

	gtk_widget_queue_draw (GTK_WIDGET(tfp->envelope));
}
